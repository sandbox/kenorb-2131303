<?php

/**
 * Settings form to select fields to synchronise for a product class.
 */
function uc_attribute_sync_class_settings_form(&$form_state, $class) {
  $attributes = uc_class_get_attributes($class->pcid);
  $fields = array('' => t('Do not synchronise'));
  $type = content_types($class->pcid); // FIXME: convert to Drupal 7
  foreach ($type['fields'] as $field) {
    if ($field['multiple']) {
      $fields[$field['field_name']] = $field['field_name'];
    }
  }

  // Class must have attributes and at least one multiple date field.
  if (empty($attributes) || count($fields) == 1) {
    $form['error'] = array(
      '#value' => '<p>' . t('The product class must have at least one attribute and one multiple-value CCK field in order to use synchronisation.') . '</p>',
    );
  }
  else {
    $form['uc_attribute_sync_' . $class->pcid] = array(
      '#tree' => TRUE,
    );

    $defaults = uc_attribute_sync_fields($class->pcid);
    foreach ($attributes as $attribute) {
      $form['uc_attribute_sync_' . $class->pcid][$attribute->aid] = array(
        '#type' => 'select',
        '#title' => $attribute->name,
        '#options' => $fields,
        '#default_value' => isset($defaults[$attribute->aid]) ? $defaults[$attribute->aid] : '',
      );
    }
  }

  return system_settings_form($form);
}
